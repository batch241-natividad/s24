//Mini-Activity

/*
1. Using the ES6 update, get the cube of 8.
2. Print the result on the console with the message: 'The interest rate on your savings account is' + result

*/



const newNum = 8 ** 3;
console.log(`The cube of 8 is ${newNum}`)

//Mini-activity
/*
Destructure the address array
print the values in the console: I live at 258 Washing Avenue, California,90011
Use Template Literals

*/
const address = ["258","Washington Ave NW", "California", "90011"]

const [streetNumber, Avenue, City, zipcode] = address
console.log(`I live at ${streetNumber} ${Avenue} ${City} ${zipcode}`)

//Mini-activity
/*
1.Destructure the animal array
2. Print the values in the console: 'Lolong was a saltwater crocodile. He weighed at 1075 kgs with a measurement of 20ft 3 in.'
*/
const animal = {
	name: "Lolong",
	species:"saltwater crocodile",
	weight:"1075 kgs",
	measurement:'20ft 3in'
}
const {name, species, weight, measurement} = animal
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}`)

//MINI - ACTIVITY
/*
1. Loop through the numbers using forEach
2.Print the numbers in the console
3. Use the reduce operator on the numbers array
4. Assign the result on a variable
5. Print the variable

*/


let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => 
	console.log(`${number}`))


const result = numbers.reduce((x, y) => x + y);
console.log(result);

//MINI-ACTIVITY
/*
1. Create a "dog" class
2.Inside of the class "dog", have a name, age, and breed
3.Instantiate a new dog class and print in the console
4.Send the screenshot of the output on hangouts
*/


class dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
let myDog = new dog("Grizz", 1, "maltese")
console.log(myDog)