//ES6 Updates

//Exponent Operator

//Old

/*const Oldnum = math.pow(8, 2)
console.log(oldNum);
*/
//new

console.log('Result of ES6: ')
const newNum = 8 ** 2;
console.log(newNum);

//TEMPLATE LITERALS

/*
-Allows us to write strings without using the concatenate operator(+)

*/

let studentName = 'Roland';

//Pre-Template Literal String
console.log("hello " + studentName +"! Welcome to programming")

//Template Literal String
console.log('Template literal: ')
console.log(`Hello ${studentName}! Welcome to programming`)

//Multi-line template literal

const message = `
${studentName} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${newNum}. `

console.log(message);


/*
-Template literals allow us to write strings with embedded Javascript expressions
-"${}" are used to include Javascript expressions in strings using template literals

*/

const interestRate = .1;
const principal = 1000;

console.log(`The interest rate on your savings account is: ${principal * interestRate}`);

//Array destructuring

/*
-Allows us to unpack elements in arrays into distinct variables
-Allows us to name array elements with variables instead of using index numbers
-Syntax:
	let / const [variableName, variableName, variableName] = array;

*/

console.log("Pre-Array Destructuring: ")

const fullName = ["Jeru", "Nebur", "Palma"]

//Pre-Array Destructuring
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`)
//Array Destructuring
console.log("Array Destructuring: ")
const [firstName, middleName, lastName] = fullName

console.log(firstName)
console.log(middleName)
console.log(lastName)
console.log(`hello ${firstName} ${middleName} ${lastName}! it's nice to meet you!`)

//OBJECT DESTRUCTURING
/*
	-Allows us to unpack properties on objects into distinct values
	-Shortens the syntax for accessing properties from objects 
	-Syntax:

		let / const{ propertyName, propertyName, propertyName, propertyName,} = object;

*/

const person ={
	givenName:"Jane",
	maidenName:"Dela",
	familyName:"Cruz",
}

//Pre-Object Destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

console.log(`Hello ${person.givenName} ${person.maidenName}. ${person.familyName}`)
//Object Destructuring
const {givenName, maidenName, familyName} = person
console.log(givenName)
console.log(maidenName)
console.log(familyName)

console.log(`Hello ${givenName} ${maidenName} ${familyName}`)
console.log("")


// ARROW FUNCTIONS

/*
-Compact alternative syntax to traditional functions
-Useful for creating code snippets where creating functions will not be reused in any other portions of the code
-Adheres to the dry principle (Don't repeat yourself) principle where when there is no longer a need to create a new function and think of a name for functions that will only be used in certain code snippets.
*/

const hello = () => {
	console.log('Goodmorning Batch 241')
};

/*
Syntax:
	let / const variableName = (parameterA, parameterB, parameterC) => {
		console.log();
	}

*/

const printFullName = (firstN, middleN, lastN) => {
	console.log(`${firstN} ${middleN} ${lastN}` )
}

printFullName('John', 'D', 'Smith');
printFullName('Eric', 'A.', 'Andales');

const students = ['John', 'Jane', 'Smith'] 
console.log("")
//arrow function with loops



students.forEach((student) => {
	console.log(`${student} is a student.`)
})


//Implicit return statement

/*
- There are instances when you can omit the "return" statement
- This works because even without the return statement, Javascript adds for it for the result of the function
*/

//Old / Pre - Arrow Function
const add = (x, y) => {

	return x + y
}
let total = add(1, 2)
console.log(total);


//Arrow function
const subtract = (x, y) => x - y
let difference = subtract(3, 1)
console.log(difference)
console.log("")

//Default argument value
//Provides a default argument if none is provided when the function is invoked.
console.log('Default argument value')
const greet = (name = 'user') => {
	return `Goodmorning, ${ name }`
}

console.log(greet())
console.log(greet('jeru'));

//CLASS-BASED OBJECT BLUEPRINTS
//Allows creation / instantiation of objects as blueprints

//creating a class

/*
- The constructor function is a special method of class for creating / initializing an object for that class.

- The "this" keyword refers to the properties initialized from the inside the class

- Syntax:
	class className {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropoertyB;
			}
	}
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

/*
- The "new" operator creates instantiates a new object with the given arguments as the value
-No arguments provided will create an object without any values assigned to its properties
- Syntax:
	let / const variableName = new className()
*/

console.log("")
console.log("Class Blueprints")
let myCar = new Car()
console.log(myCar)
console.log("")


//Assigning properties after creation / instantiation of an object

myCar.brand = 'Ford';
myCar.name = 'Ranger raptor';
myCar.year = 2021;
console.log(myCar);

//Instantiate a new object

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);